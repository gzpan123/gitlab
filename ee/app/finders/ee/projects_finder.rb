# frozen_string_literal: true

module EE
  # ProjectsFinder
  #
  # Extends ProjectsFinder
  #
  # Added arguments:
  #   params:
  #     plans: string[]
  #     feature_available: string[]
  #     aimed_for_deletion: Symbol
  #     include_hidden: boolean
  #     filter_expired_saml_session_projects: boolean
  module ProjectsFinder
    extend ::Gitlab::Utils::Override

    private

    override :filter_projects
    def filter_projects(collection)
      collection = super(collection)
      collection = by_plans(collection)
      collection = by_feature_available(collection)
      collection = by_hidden(collection)
      collection = by_marked_for_deletion_on(collection)
      collection = by_saml_sso_session(collection)
      by_aimed_for_deletion(collection)
    end

    def by_saml_sso_session(projects)
      return projects unless filter_expired_saml_session_projects?

      projects.by_not_in_root_id(current_user.expired_sso_session_saml_providers.select(:group_id))
    end

    def filter_expired_saml_session_projects?
      return false if current_user.nil? || current_user.can_read_all_resources?

      params.fetch(:filter_expired_saml_session_projects, false)
    end

    def by_plans(collection)
      if names = params[:plans].presence
        collection.for_plan_name(names)
      else
        collection
      end
    end

    def by_feature_available(collection)
      if feature = params[:feature_available].presence
        collection.with_feature_available(feature)
      else
        collection
      end
    end

    def by_marked_for_deletion_on(collection)
      return collection unless params[:marked_for_deletion_on].present?
      return collection unless License.feature_available?(:adjourned_deletion_for_projects_and_groups)

      collection.by_marked_for_deletion_on(params[:marked_for_deletion_on])
    end

    def by_aimed_for_deletion(items)
      if ::Gitlab::Utils.to_boolean(params[:aimed_for_deletion])
        items.aimed_for_deletion(Date.current)
      else
        items
      end
    end

    def by_hidden(items)
      params[:include_hidden].present? ? items : items.not_hidden
    end
  end
end
